<?php /* Template Name: Home */
get_header();

$latest_portfolio = get_posts(array(
    'post_type' => 'fw_portfolio',
    'post_status' => 'publish',
    'numberposts' => -1
));

if(sizeof($latest_portfolio) > 0) { ?>

    <div class="masonry">
        <div class="grid-sizer"></div>

        <?php foreach ($latest_portfolio as $post) { setup_postdata($post); ?>

            <?php get_template_part('templates/single/work'); ?>

        <?php } wp_reset_postdata(); ?>

    </div>

<?php } else { ?>

    <?php get_template_part('content','none'); ?>

<?php } ?>

<?php get_footer(); ?>
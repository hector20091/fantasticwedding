<?php global $post;

$page_id = $post->ID;

$post_categories = wp_get_post_terms($page_id,'fw_portfolio_cat');

$cat_display = '';
$categories = array();
if(sizeof($post_categories) > 0) {
    foreach ($post_categories as $category) {
        //$categories[] = '<a href="'.get_term_link($category).'">'.$category->name.'</a>';
        $categories[] = $category->name;
    }

    $cat_display = '<span class="details">- '.join(', ',$categories).'</span>';
}

$thumbnails = get_field('portfolio_thumbnails',$page_id);

?>


<div class="grid-item">

    <a href="<?php the_permalink(); ?>">

        <?php if(!empty($thumbnails)) { ?>

            <div class="image">

                <?php foreach ($thumbnails as $key => $image) {
                    $class = ($key%2==0)?'front':'back'; ?>

                    <img src="<?php echo $image['url']; ?>" alt="" class="<?php echo $class; ?>" />

                <?php } ?>

            </div>

        <?php } ?>

        <span class="caption"><?php the_title(); ?> <?php echo $cat_display; ?></span>
    </a>

</div>
<?php $sections = get_field('contact_sections','option');

if(!empty($sections)) { ?>

    <div id="contact">
        <a href="javascript:;" class="close"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-close.png" alt="" /></a>
        <div class="table">
            <div class="cell">

                <?php foreach ($sections as $section) {
                    $layout = $section['acf_fc_layout'];
                    $title = $section['title'];
                    $description = $section['description'];
                    $description_left = $section['description_left'];
                    $description_right = $section['description_right'];

                    if($layout == 'one_column') { ?>

                        <div class="large">

                            <?php echo $description; ?>

                        </div>

                    <?php } else if($layout == 'two_columns') { ?>

                        <?php if(!empty($title)) { ?>

                            <p class="large lessmarg"><?php echo $title; ?></p>

                        <?php } ?>

                        <div class="cols">
                            <div class="col one_half">

                                <?php echo $description_left; ?>

                            </div>
                            <div class="col one_half">

                                <?php echo $description_right; ?>

                            </div>
                        </div>

                        <p>&nbsp;</p>

                    <?php } ?>

                <?php } ?>

            </div>
        </div>
    </div>

<?php } ?>
<?php /* Template Name: About */
get_header(); ?>

    <div class="page about">
        <div class="section">
            <div class="cols">
                <div class="col one_half">
                    <img src="http://oneandother.co/wp-content/themes/one-and-other/img/about-02.jpg" alt="" class="show_on_mobile" />
                    <div class="show_on_mobile padded">
                        <p class="large">One &amp; Other is a design studio in Charleston, SC, led by Will Allport and Ky Coffman. They work across a range of industries to produce conceptually rich, aesthetically timeless designs.</p>
                        <p class="nomarg">With backgrounds in architecture and the fine arts, they bring a deep skill set and holistic approach to work realized on paper, through digital experience and in the built environment. Their work strives to find balance at the intersection of function and beauty, masculine and feminine, old and new.</p>
                    </div>
                    <img src="http://oneandother.co/wp-content/themes/one-and-other/img/about-01.jpg" alt="" />
                    <div class="padded">
                        <p class="large">Capabilities</p>
                        <div class="cols">
                            <div class="col one_half">
                                <p class="nomarg">Branding &amp; Identity<br />
                                    Web Design<br />
                                    Responsive Development<br />
                                    E-Commerce<br />
                                    Art Direction<br />
                                    Print Stationery</p>
                            </div>
                            <div class="col one_half">
                                <p class="nomarg">Packaging</p>
                                <p class="nomarg">Strategy</p>
                                <p class="nomarg">Illustration</p>
                                <p class="nomarg">Custom Typography</p>
                                <p class="nomarg">Experiential<br />
                                    Signage &amp; Wayfinding</p>
                            </div>
                        </div>
                    </div>
                    <img src="http://oneandother.co/wp-content/themes/one-and-other/img/about-03.jpg" alt="" />
                </div>
                <div class="col one_half">
                    <div class="hide_on_mobile padded">
                        <p class="large">One &amp; Other is a design studio in Charleston, SC, led by Will Allport and Ky Coffman. They work across a range of industries to produce conceptually rich, aesthetically timeless designs.</p>
                        <p class="nomarg">With backgrounds in architecture and the fine arts, they bring a deep skill set and holistic approach to work realized on paper, through digital experience and in the built environment. Their work strives to find balance at the intersection of function and beauty, masculine and feminine, old and new.</p>
                    </div>
                    <img src="http://oneandother.co/wp-content/themes/one-and-other/img/about-02.jpg" alt="" class="hide_on_mobile" />
                    <div class="padded">
                        <p class="large lessmarg">Interested in working together?</p>
                        <div class="cols">
                            <div class="col one_half">
                                <p>New work:<br />
                                    <a href="mailto:hello@oneandother.co">hello@oneandother.co</a></p>
                            </div>
                            <div class="col one_half">
                                <p>Join our team:<br />
                                    <a href="mailto:work@oneandother.co">work@oneandother.co</a></p>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <p class="large lessmarg">Join us online</p>
                        <div class="cols">
                            <div class="col one_half">
                                <p>Instagram:<br />
                                    <a href="https://www.instagram.com/one_and_other/" target="_blank">One_And_Other</a></p>
                            </div>
                            <div class="col one_half">
                                <p>Twitter:<br />
                                    <a href="https://twitter.com/one_and_other" target="_blank">One_And_Other</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
<?php

function portfolio_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Portfolio' ),
            'singular_name' => __( 'Portfolio' )
        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'portfolio' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' )
    );

    register_post_type( 'fw_portfolio', $args );

    register_taxonomy(
        'fw_portfolio_cat',
        'fw_portfolio',
        array(
            'hierarchical'  => true,
            'label'         => __( 'Categories' ),
            'singular_name' => __( 'Category' ),
            'rewrite'       => array( 'slug' => 'portfolio-cat' ),
        )
    );
}
add_action('init','portfolio_post_type_init');
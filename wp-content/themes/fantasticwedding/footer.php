
    <?php if(is_front_page()) { ?>

        <div class="footer">
            <p class="large">Say <a href="mailto:hello@oneandother.co">hello@oneandother.co</a> or<br />join us on <a href="https://www.instagram.com/one_and_other/" target="_blank">Instagram</a> and <a href="#">Twitter</a>.</p>
        </div>

    <?php } ?>

</div>



<div id="mobile_menu">
    <div class="inner">

        <?php
        wp_nav_menu( array(
            'theme_location' => 'primary',
            'menu_class'     => 'primary-menu',
            'container'      => ''
        ) );
        ?>

        <a href="javascript:;" class="mobile_close"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-close.png" alt="" /></a>
    </div>
</div>

<?php get_template_part('templates/popups/contact'); ?>

<?php wp_footer(); ?>

</body>
</html>

<?php get_header();

$page_id = get_the_ID();

$gallery = get_field('portfolio_gallery',$page_id);

if(!empty($gallery)) { ?>

    <div class="masonry single">
        <div class="grid-sizer"></div>

        <?php foreach ($gallery as $key => $image) { ?>

            <div class="grid-item">
                <img src="<?php echo $image['url']; ?>" alt="" class="thumbnail" data-row="<?php echo $key+1; ?>" />
            </div>

        <?php } ?>

    </div>

    <div class="project" style="display:none;">

        <?php foreach ($gallery as $key => $image) { ?>

            <div class="row" id="row_<?php echo $key+1; ?>">
                <img src="<?php echo $image['url']; ?>" alt="" />
            </div>

        <?php } ?>

    </div>

    <div class="toggle_view grid"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-grid.png" alt="" /></div>
    <div class="back_to_top"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-arrow-up.png" alt="" /></div>

<?php } else { ?>

    <?php get_template_part('content','none'); ?>

<?php } ?>


<?php get_footer(); ?>
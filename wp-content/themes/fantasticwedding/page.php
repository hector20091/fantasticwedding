<?php get_header();

$page_id = get_the_ID();

$sections = get_field('sections',$page_id);

//echo '<pre>';print_r($sections);echo '</pre>';?>

    <div class="page about">
        <div class="section">

            <?php if(!empty($sections)) { ?>

                <div class="cols">

                    <?php foreach ($sections as $section) {
                        $layout = $section['acf_fc_layout'];
                        $column_left = $section['column_left'];
                        $column_right = $section['column_right'];

                        if($layout == 'two_columns') { ?>

                            <?php if(!empty($column_left)) { ?>

                                <div class="col one_half">

                                    <?php foreach ($column_left as $column) {
                                        $c_image = $column['image'];
                                        $c_title = $column['title'];
                                        $c_description = $column['description'];

                                        if(!empty($c_image)) { ?>

                                            <img src="<?php echo $c_image; ?>" alt="">

                                        <?php } ?>

                                        <?php if(!empty($c_title) || !empty($c_description)) { ?>

                                            <div class="padded">

                                                <?php if(!empty($c_title)) { ?>

                                                    <p class="large"><?php echo $c_title; ?></p>

                                                <?php } ?>

                                                <?php if(!empty($c_description)) { ?>

                                                    <div class="column-description">

                                                        <?php echo $c_description; ?>

                                                    </div>

                                                <?php } ?>

                                            </div>

                                        <?php } ?>

                                    <?php } ?>

                                </div>

                            <?php } ?>

                            <?php if(!empty($column_right)) { ?>

                                <div class="col one_half">


                                    <?php foreach ($column_right as $column) {
                                        $c_image = $column['image'];
                                        $c_title = $column['title'];
                                        $c_description = $column['description'];

                                        if(!empty($c_image)) { ?>

                                            <img src="<?php echo $c_image; ?>" alt="">

                                        <?php } ?>

                                        <?php if(!empty($c_title) || !empty($c_description)) { ?>

                                            <div class="padded">

                                                <?php if(!empty($c_title)) { ?>

                                                    <p class="large"><?php echo $c_title; ?></p>

                                                <?php } ?>

                                                <?php if(!empty($c_description)) { ?>

                                                    <div class="column-description">

                                                        <?php echo $c_description; ?>

                                                    </div>

                                                <?php } ?>

                                            </div>

                                        <?php } ?>

                                    <?php } ?>

                                </div>

                            <?php } ?>

                        <?php }
                    } ?>

                </div>

            <?php } else { ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <h1 class="large"><?php the_title(); ?></h1>

                    <?php the_content(); ?>

                <?php endwhile; ?>

            <?php } ?>

        </div>
    </div>

<?php get_footer(); ?>
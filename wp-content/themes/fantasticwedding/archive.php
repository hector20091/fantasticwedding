<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

    <div class="masonry">
        <div class="grid-sizer"></div>

        <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part('templates/single/work'); ?>

        <?php endwhile; ?>

    </div>

<?php else :

    get_template_part( 'content', 'none' );

endif; ?>

<?php get_footer(); ?>

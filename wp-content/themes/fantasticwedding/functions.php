<?php

include 'includes/aq_resizer.php';
include 'includes/post-types.php';

/**
 * Fantastic Wedding functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Fantastic Wedding 1.0
 */

if ( ! function_exists( 'fantasticwedding_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Fantastic Wedding 1.0
 */
function fantasticwedding_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/fantasticwedding
	 * If you're building a theme based on fantasticwedding, use a find and replace
	 * to change 'fantasticwedding' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'fantasticwedding' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'fantasticwedding' ),
		'social'  => __( 'Social Links Menu', 'fantasticwedding' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
}
endif; // fantasticwedding_setup
add_action( 'after_setup_theme', 'fantasticwedding_setup' );

/**
 * Register widget area.
 *
 * @since Fantastic Wedding 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function fantasticwedding_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'fantasticwedding' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'fantasticwedding' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'fantasticwedding_widgets_init' );

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Fantastic Wedding 1.1
 */
function fantasticwedding_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'fantasticwedding_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Fantastic Wedding 1.0
 */
function fantasticwedding_scripts() {

    wp_deregister_script('jquery');

    wp_register_script('js-fonts', '//fast.fonts.net/jsapi/3241453d-7696-41fe-ad09-bc6434f0124a.js');
    wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js');
    wp_register_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array('jquery'), false, true);
    wp_register_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), false, true);
    wp_register_script('owl-carousel', get_template_directory_uri() . '/js/owl.js', array('jquery'), false, true);
    wp_register_script('fantasticwedding', get_template_directory_uri() . '/js/scripts.js', array('jquery'), false, true);

    wp_enqueue_script('js-fonts');
    wp_enqueue_script('jquery');
    wp_enqueue_script('imagesloaded');
    wp_enqueue_script('masonry');
    wp_enqueue_script('owl-carousel');
    wp_enqueue_script('fantasticwedding');

	// Load our main stylesheet.
	wp_enqueue_style( 'fantasticwedding-style', get_stylesheet_uri() );

    // Scripts
    wp_enqueue_style( 'fantasticwedding-style-mobile', get_template_directory_uri() . '/style-mobile.css' );
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/js/owl.carousel.css' );

	/*wp_enqueue_script( 'fantasticwedding-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );
	wp_localize_script( 'fantasticwedding-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'fantasticwedding' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'fantasticwedding' ) . '</span>',
	) );*/
}
add_action( 'wp_enqueue_scripts', 'fantasticwedding_scripts' );

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();

}
/*var tooltipSpan = document.getElementById('tooltip-span');

window.onmousemove = function(e) {
    var x = e.clientX,
        y = e.clientY;
    tooltipSpan.style.top = (y + 20) + 'px';
    tooltipSpan.style.left = (x + 20) + 'px';
};*/

jQuery(function($){
	
	resizeWindow();
	  		
 	$(window).resize(resizeWindow);
  
    $(window).scroll(function() {
		headerSetup();
	});
  
	// animate nav
	$('.nav').addClass('moved');
	
	// masonry - on window load
	$(window).load(function() {
		var $grid = $('.masonry').masonry({
			itemSelector: '.grid-item',
			columnWidth: '.grid-sizer',
			percentPosition: true
		});
		$grid.imagesLoaded().progress( function() {
			$grid.masonry('layout');
		});
	});
	
	// single project thumbnail image click
	$('.masonry .thumbnail').click(function() {
		var row = $(this).data('row');
		$('.toggle_view').removeClass('grid');	
		$('.masonry').fadeOut(300, function() {
			$('.project').fadeIn(300, function() {  
				$('html, body').animate({
					scrollTop: $('#row_' + row).offset().top
				}, 700);
			});
		});
	});
		
	// all other image sliders
	$('.slider').each(function() {
		var owl = $(this);								 
		owl.owlCarousel({
			items: 1, 
			loop: true, 
			autoplaySpeed: 1000, 
			dots: false,
			autoplay: true,
			autoplayTimeout: 6000,
			autoplayHoverPause: true,
			animateOut: 'fadeOut'
		});
	});
	
	// grid view toggler
	$('.toggle_view').click(function() {
		var $this = $(this);
		if ($this.hasClass('grid')) {
			$this.removeClass('grid');	
			$('.masonry').fadeOut(300, function() {
				$('.project').fadeIn(300);
			});
		} else {
			$this.addClass('grid');	
			$('.project').fadeOut(300, function() {
				$('.masonry').fadeIn(300, function() {
					$(this).masonry('layout');						   
				});
			});
		}
	});
	
	// back to top
	$('.back_to_top').click(function() {
        $('html, body').animate({'scrollTop': 0}, 700);
    });
	
	// lazyload?
	
	// overlay transition 
	$('.nav li.transition a, .grid-item a').click(function(){
		if ($(this).parent().hasClass('transition')) {
			$('#overlay').fadeIn(600);
		} else {
			// if immediate li parent isn't .transition, don't transition	
		}
	});
	
	// contact
	$('.nav li.contact_open a, #mobile_menu li.contact_open a').click(function(e){
		e.preventDefault();
		$('#contact').fadeIn(600, function() {
			$('body').css('overflow','hidden');								
		});
	});
	$('#contact .close').click(function(e){
		e.preventDefault();
		$('#contact').fadeOut(600, function() {
			$('body').css('overflow','auto');								 
		});
	});
	
	// mobile menu
	$('.mobile_toggle').click(function(e) {
		e.preventDefault();
		$('#mobile_menu').fadeIn(300, function() {
			$('body').addClass('locked');
			$('#mobile_menu .inner').addClass('open');	
		});
	});
	$('.mobile_close').click(function(e) {
		e.preventDefault();
		$('#mobile_menu .inner').animate({'right':'-256px'}, 'slow').removeClass('open');	
		$('#mobile_menu').fadeOut(300, function() {
			$('body').removeClass('locked');									   
		});
	});
		
	// overlay fade
	// http://stackoverflow.com/questions/8788802/prevent-safari-loading-from-cache-when-back-button-is-clicked - rather than using $(window).pageshow(function(){
	$(window).bind('pageshow', function(event) {
		if ($('#overlay').length) {
			$('#overlay').delay(1000).fadeOut(600);
		}
		if ($('#super_overlay').length) {
			//$('body').css('background','#FFFFFF');
			var $this = $('#super_overlay');
			$('#super_overlay').delay(500).removeClass('preload').delay(500).queue(function(next) {
				$('#super_overlay .table .cell span').fadeIn(300, function() {
					$(this).stop();
					$this.stop().delay(500).fadeOut(600);
				});																	  
			});
		}
	});
	
});

function resizeWindow() {

	var ww = $(window).width();
	
	/* new */
	var main_pad_left = parseInt($('#main').css('padding-left'));
	var main_width = parseInt($('#main').width());
	var main_left_plus_width = main_pad_left + main_width;
	var right = ((ww - main_left_plus_width) /  2);
	$('.toggle_view, .back_to_top').css('right', right + 'px');
	/* end new */
			
	/*if (ww > 1735) {
	
		// default padding left: 200
		// default padding right: 95
		
		var half_diff = (ww - 1735) / 2;
		var padding_left = 200 + half_diff;
		var padding_right = 95 + half_diff;
		
		$('#main').css({
			'max-width' : 'none',
			'padding-left' : padding_left + 'px',
			'padding-right' : padding_right + 'px'
		}); 	
		
		$('#logo').css('width', padding_left + 'px');	
		
		if ($('.toggle_view').length) {
	
			var right = (ww - (1440 + padding_left)) / 2;
			
			$('.toggle_view, .back_to_top').css('right', right + 'px');
		
		}
			
	} else {
		
		$('#main').css({
			'max-width' : '1735px',
			'padding' : '70px 95px 70px 200px'
		});
		$('#logo').css('width', '200px');
		
		if ($('.toggle_view').length) {
			$('.toggle_view, .back_to_top').css('right', '48px');	
		}
		
	}*/
			
}

function headerSetup(){
		
    var scrollPos = $(document).scrollTop();
  
    if (scrollPos > 500) {
       $('.back_to_top').fadeIn(300); 
    } else {
       $('.back_to_top').fadeOut(300); 
    }

}
